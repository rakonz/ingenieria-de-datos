# Proyecto Ingeniería de Datos

#### Beer Reviews

***Autores:*** Francisco Cid, Saúl Cifuentes

 El presente dataset contiene alrededor de 1.500.000 reviews de cervezas, en donde se identifican 104 estilos de cervezas. Fuente: [BeerReviews Kaggle.](https://https://www.kaggle.com/datasets/rdoume/beerreviews)
 
 El objetivo del proyecto es poder clasificar los tipos de cervezas que serían más atractivas para el público en general. Además de analizar qué atributos son los que tienen mayor correlación, así entender de qué forma es que una cerveza es mayor apreciada por los consumidores.